-----Sokoban game made by the bits_please team for the course "Software project laboratory"-----


<p align="center"><img src="https://gitlab.com/bits_please/sokoban/raw/master/egyeb/title_screen.bmp" /></p>

The final documentation of the project can be found here: [DOCUMENTATION](./DOCUMENTATION.pdf) - we recommend downloading it, GitLab pdf viewer is a bit slow.


The course demanded the mixed use of English and Hungarian, so unfortunately it isn't plain english. A quick scroll-through can give an idea of the types of diagrams used,
while most notable diagrams can be found in this repo also:

[Business Logic class diagram](./Class diagram.bmp)

[UI class diagram](./UI_Class_Diagram.bmp)

Folder containing other diagrams:

[egyeb (other)](./egyeb)


Screenshot of the game:

<p align="center"><img src="https://gitlab.com/bits_please/sokoban/raw/master/egyeb/game_screenshot.PNG" /></p>
