package hu.bitsplease.teszt;
import hu.bitsplease.service.*;

import java.util.ArrayList;
import java.util.List;
import static hu.bitsplease.service.Direction.*;

public class TestUtil {

    public static void listTiles(List<Tile> tiles){
        //példa kimenet:
        //
        //<none> - <Worker> Neighbours:
        //  U: <none>
        //  L: <none>
        //  D: <Switch, Honey> - <Crate>
        //  R: <SwitchHole> - <none>
        //...
        for (Tile tile : tiles) {
            printTile(tile);
        }
    }

    private static void printTile(Tile tile){
        System.out.print("<" + readITOs(tile) + "> - <" + readMTO(tile) + "> Neighbours:\nU: ");
        if(tile.getNeighbours().containsKey(UP) && !(tile.getNeighbours().get(UP).getImmovableObjects().isEmpty() && tile.getNeighbours().get(UP).getMovableObject() == null)){
            System.out.print("<" + readITOs(tile.getNeighbourTile(UP)) + "> - <" + readMTO(tile.getNeighbourTile(UP)) + ">\nL: ");
        }else{
            System.out.print("<none>\nL: ");
        }
        if(tile.getNeighbours().containsKey(LEFT) && !(tile.getNeighbours().get(LEFT).getImmovableObjects().isEmpty() && tile.getNeighbours().get(LEFT).getMovableObject() == null)){
            System.out.print("<" + readITOs(tile.getNeighbourTile(LEFT)) + "> - <" + readMTO(tile.getNeighbourTile(LEFT)) + ">\nD: ");
        }else{
            System.out.print("<none>\nD: ");
        }
        if(tile.getNeighbours().containsKey(DOWN) && !(tile.getNeighbours().get(DOWN).getImmovableObjects().isEmpty() && tile.getNeighbours().get(DOWN).getMovableObject() == null)){
            System.out.print("<" + readITOs(tile.getNeighbourTile(DOWN)) + "> - <" + readMTO(tile.getNeighbourTile(DOWN)) + ">\nR: ");
        }else{
            System.out.print("<none>\nR: ");
        }
        if(tile.getNeighbours().containsKey(RIGHT) && !(tile.getNeighbours().get(RIGHT).getImmovableObjects().isEmpty() && tile.getNeighbours().get(RIGHT).getMovableObject() == null)){
            System.out.print("<" + readITOs(tile.getNeighbourTile(RIGHT)) + "> - <" + readMTO(tile.getNeighbourTile(RIGHT)) + ">\n");
        }else{
            System.out.print("<none>\n");
        }
    }

    private static String readITOs(Tile t){
        String immovable = "";
        if(t.getImmovableObjects() != null){
            List<ImmovableTileObject> ito = t.getImmovableObjects();
            //ha nem üres az ito lista, minden ito nevét kiírjuk
            if(!ito.isEmpty()) {
                for(ImmovableTileObject i : ito){
                    immovable += i.getClass().getSimpleName() + ", ";
                }
                //lecsapjuk a végéről a vesszőt és a szóközt
                immovable = immovable.substring(0, immovable.length()-2);
            } else {
                immovable = "none";
            }
        }

        return immovable;
    }

    private static String readMTO(Tile t){
        MovableTileObject mto = t.getMovableObject();
        if (mto != null) {
            return t.getMovableObject().getClass().getSimpleName();
        } else {
            return "none";
        }
    }

    public static void listCratePositionsAndDestinations(List<Tile> tiles){
        for(Tile t : tiles){
            if(t.getMovableObject() instanceof Crate){
                System.out.print("<Crate>,");
                printTile(t);
            }else{
                for(ImmovableTileObject ito : t.getImmovableObjects()){
                    if(ito instanceof CrateDestination){
                        System.out.print("<CrateDestination>,");
                        printTile(t);
                    }
                }
            }
        }
    }

    public static void listWorkerPositions(List<Player> players){
        for(Player p : players){
            if(p.getWorker() != null){
                System.out.print("<Worker>,");
                printTile(p.getWorker().getTile());
            }
        }
    }

    public static void listPlayers(List<Player> players){
        for(int i = 0;i < players.size(); ++i){
            System.out.print("<" + (i+1) + "> - <" + players.get(i).getName() + ">\n");
        }
    }

    public static void listPoints(List<Player> players){
        for (int i = 0; i < players.size(); ++i) {
            System.out.print("<" + (i+1) + "> - <" + players.get(i).getName() + ">: <" + players.get(i).getPoints() + ">\n");
        }
    }
    public static List<String> getTestResultStringList(List<String> tf,String section){
        List<String> res = new ArrayList<String>();
        int i = tf.indexOf("<"+section+">")+1;
        while ((tf.indexOf("<\\"+section+">") > i)) {
            res.add(tf.get(i));
            i++;
        }
        return res;
    }
}
