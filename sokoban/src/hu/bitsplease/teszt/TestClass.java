package hu.bitsplease.teszt;

import hu.bitsplease.cli.Main;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static hu.bitsplease.cli.Main.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
public class TestClass {

    List<String> output;
    private List<String> testfile;

    @Before
    public void setUp() throws Exception {
        Main.loadMap("testmap");
        Main.startGame("testMap");
        output = new ArrayList<String>();

        Scanner s = new Scanner(new File("testfile.txt"));
        testfile = new ArrayList<String>();
        while (s.hasNextLine()){
            testfile.add(s.nextLine());
        }
        s.close();
    }

    @Test
    public void startGame(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_startGame_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_startGame_WorkerPositions"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_startGame_CratePosAndDes"), is(output));

        output.clear();
        listPlayers(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_startGame_Players"), is(output));

        output.clear();
        listPoints(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_startGame_Points"), is(output));
    }

    @Test
    public void givePointsForPushingCrateToCrateDestination(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_WorkerPositions1"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_CratePosAndDes"), is(output));

        output.clear();
        listPoints(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_Points1"), is(output));

        move("Player 1", "LEFT");
        move("Player 1", "UP");
        move("Player 0", "RIGHT");

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_WorkerPositions2"), is(output));

        output.clear();
        listPoints(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_givePointCtoCD_Points2"), is(output));
    }

    @Test
    public void workerCannotStepOnOtherWorkersTile(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnOWorker_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnOWorker_WorkerPositions1"), is(output));

        move("Player 0", "DOWN");
        move("Player 0", "DOWN");
        move("Player 0", "RIGHT");
        move("Player 0", "RIGHT");

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnOWorker_WorkerPositions2"), is(output));
    }

    @Test
    public void workerCannotStepOnWall(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnWall_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnWall_WorkerPositions1"), is(output));

        move("Player 0", "UP");

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCantStepOnWall_WorkerPositions2"), is(output));
    }

    @Test
    public void crateDoesNotDieOnDeactivatedSwitchHole(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDoesntDieOnDeactSHole_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDoesntDieOnDeactSHole_WorkerPositions1"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDoesntDieOnDeactSHole_CratePosAndDes1"), is(output));

        move("Player 1", "LEFT");
        move("Player 1", "LEFT");
        move("Player 1", "LEFT");
        move("Player 1", "UP");

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDoesntDieOnDeactSHole_CratePosAndDes2"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDoesntDieOnDeactSHole_WorkerPositions2"), is(output));
    }

    @Test
    public void crateDiesOnActivatedSwitchHole(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDiesOnActSHole_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDiesOnActSHole_WorkerPositions1"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDiesOnActSHole_CratePosAndDes1"), is(output));

        move("Player 1", "LEFT");
        move("Player 1", "UP");
        move("Player 0", "DOWN");
        move("Player 0", "DOWN");
        move("Player 0", "LEFT");
        move("Player 0", "UP");

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDiesOnActSHole_CratePosAndDes2"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateDiesOnActSHole_WorkerPositions2"), is(output));
    }

    @Test
    public void workerDiesOnActivatedSwitchHole(){
//        output.clear();
//        listTiles(true, output);
//        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerDiesOnActSHole_Tiles"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerDiesOnActSHole_WorkerPositions1"), is(output));

        move("Player 1", "LEFT");
        move("Player 1", "UP");
        move("Player 0", "LEFT");

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerDiesOnActSHole_WorkerPositions2"), is(output));
    }

    @Test
    public void crateBecomesWallOnCrateDestination(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateBecomesWallOnCD_Tiles1"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateBecomesWallOnCD_CratePosAndDes1"), is(output));

        move("Player 1", "LEFT");
        move("Player 1", "UP");
        move("Player 0", "RIGHT");

        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateBecomesWallOnCD_Tiles2"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_crateBecomesWallOnCD_CratePosAndDes2"), is(output));
    }

    @Test
    public void workerCanPushCrate(){
        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_Tiles1"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_CratePosAndDes1"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_WorkerPositions1"), is(output));

        move("Player 0", "DOWN");
        move("Player 0", "RIGHT");

        output.clear();
        listTiles(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_Tiles2"), is(output));

        output.clear();
        listCratePositionsAndDestinations(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_CratePosAndDes2"), is(output));

        output.clear();
        listWorkerPositions(true, output);
        assertThat(TestUtil.getTestResultStringList(testfile,"Test_workerCanPushCrate_WorkerPositions2"), is(output));
    }
}