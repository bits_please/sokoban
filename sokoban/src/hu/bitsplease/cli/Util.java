package hu.bitsplease.cli;

import hu.bitsplease.service.ImmovableTileObject;
import hu.bitsplease.service.MovableTileObject;
import hu.bitsplease.service.Player;
import hu.bitsplease.service.Tile;
import java.util.List;

/**
 * Segédosztály a könnyebb olvashatóság és az egszerűbb kiíratás céljából.
 */
@Deprecated
public class Util {
    /**
     * Kiír egy tabot.
     */
    public static void printTab(){
        System.out.print("\t");
    }

    /**
     * ->
     * @param objNev típus
     * @param osztalyNev név
     * @param method függvény neve
     * @param param paraméterek
     */
    public static void printCall(String objNev, String osztalyNev, String method, String param){
        System.out.println("\t->" + "[" + objNev + " : " + osztalyNev + "]" + "." + method + "(" + param + ")");
    }

    /**
     * <-
     * @param objNev típus
     * @param osztalyNev név
     * @param method függvény neve
     * @param param paraméterek
     */
    public static void printReturn(String objNev, String osztalyNev, String method, String param){
        System.out.println("\t<-" + "[" + objNev + " : " + osztalyNev + "]" + "." + method + "(" + param + ")");
    }


}
