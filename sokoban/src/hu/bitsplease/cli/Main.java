package hu.bitsplease.cli;

import hu.bitsplease.service.*;

import hu.bitsplease.teszt.TestUtil;
import org.jdom2.input.SAXBuilder;

import javax.activation.UnsupportedDataTypeException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
@Deprecated
public class Main {
    public static void main(String[] args) {
        try{
            System.out.println("\"wall\"");
            //List<String> argList = new ArrayList<String>(Arrays.asList(args));
            List<String> argList  = new ArrayList<>();
            String in = "";
            Scanner sc = new Scanner(System.in);
            while(!in.equals("end")){
                in = sc.nextLine();
                argList.add(in);
            }
            boolean isEnd = false;
            do{
                if(!argList.isEmpty()){
                    String command = argList.get(0);
                    argList.remove(0);

                    switch(command){
                        case "loadMap":
                            loadMap(argList.get(0));
                            argList.remove(0);
                            break;
                        case "move":
                            move(argList.get(0), argList.get(1));
                            argList.remove(0);
                            argList.remove(0);
                            break;
                        case "startGame":
                            startGame(argList.get(0));
                            argList.remove(0);
                            break;
                        case "dropMovementModifier":
                            dropMovementModifier(argList.get(0), argList.get(1));
                            argList.remove(0);
                            argList.remove(0);
                            break;
                        case "listTiles":
                            listTiles(false, null);
                            break;
                        case "listPlayers":
                            listPlayers(false, null);
                            break;
                        case "listPoints":
                            listPoints(false,null);
                            break;
                        case "listCratePositionsAndDestinations":
                            listCratePositionsAndDestinations(false, null);
                            break;
                        case "listWorkerPositions":
                            listWorkerPositions(false, null);
                            break;
                        default:
                            throw new UnsupportedOperationException("Unsupported command!!!! Fatal Error!! Very Bad!!!");

                    }
                }else{
                    isEnd = true;
                }
            }while(!isEnd);
        }catch(Exception e){
            System.out.println("Fatal error while running! Stacktrace: \n");
            e.printStackTrace();
        }
        System.out.println("Bye! :D");
    }

    public static void loadMap(String map) throws IOException{
        Game.loadMap(new File("maps\\" + map + ".xml"));
    }
    public static void move(String player, String where){
        Game.playerMove(player, Direction.valueOf(where));
    }
    public static void startGame(String chosenMap) throws IOException{
        Game.chooseMap(chosenMap);
        Game.startGame();

    }
    public static void dropMovementModifier(String player, String what) throws UnsupportedDataTypeException{
        Game.dropMovementModifier(player, movementModifierFactory(what));
    }
    public static void listTiles(boolean isTest, List<String> output){
        if(isTest){
            ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(byteArrayStream);
            PrintStream old = System.out;

            System.setOut(stream);
            TestUtil.listTiles(Game.getCurrentMap().getTiles());
            System.out.flush();
            System.setOut(old);

            String[] lines = byteArrayStream.toString().split("\n");
            output.addAll(Arrays.asList(lines));
        }else{
            TestUtil.listTiles(Game.getCurrentMap().getTiles());
        }
    }
    public static void listPlayers(boolean isTest, List<String> output){
        if(isTest){
            ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(byteArrayStream);
            PrintStream old = System.out;

            System.setOut(stream);
            TestUtil.listPlayers(Game.getCurrentMap().getPlayers());
            System.out.flush();
            System.setOut(old);

            String[] lines = byteArrayStream.toString().split("\n");
            output.addAll(Arrays.asList(lines));
        }else{
            TestUtil.listPlayers(Game.getCurrentMap().getPlayers());
        }

    }
    public static void listPoints(boolean isTest, List<String> output){
        if(isTest){
            ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(byteArrayStream);
            PrintStream old = System.out;

            System.setOut(stream);
            TestUtil.listPoints(Game.getCurrentMap().getPlayers());
            System.out.flush();
            System.setOut(old);

            String[] lines = byteArrayStream.toString().split("\n");
            output.addAll(Arrays.asList(lines));
        }else{
            TestUtil.listPoints(Game.getCurrentMap().getPlayers());
        }

    }
    public static void listCratePositionsAndDestinations(boolean isTest, List<String> output){
        if(isTest){
            ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(byteArrayStream);
            PrintStream old = System.out;

            System.setOut(stream);
            TestUtil.listCratePositionsAndDestinations(Game.getCurrentMap().getTiles());
            System.out.flush();
            System.setOut(old);

            String[] lines = byteArrayStream.toString().split("\n");
            output.addAll(Arrays.asList(lines));
        }else{
            TestUtil.listCratePositionsAndDestinations(Game.getCurrentMap().getTiles());
        }

    }
    public static void listWorkerPositions(boolean isTest, List<String> output){
//        try {
//            Thread.sleep(5000);
//        }catch(Exception ex){}
        if(isTest){
            ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(byteArrayStream);
            PrintStream old = System.out;

            System.setOut(stream);
            TestUtil.listWorkerPositions(Game.getCurrentMap().getPlayers());
            System.out.flush();
            System.setOut(old);

            String[] lines = byteArrayStream.toString().split("\n");
            output.addAll(Arrays.asList(lines));
        }else{
            TestUtil.listWorkerPositions(Game.getCurrentMap().getPlayers());
        }
    }
    private static MovementModifier movementModifierFactory(String name) throws UnsupportedDataTypeException{
        switch(name){
            case "Oil":
                return new Oil();
            case "Honey":
                return new Honey();
            default:
                throw new UnsupportedDataTypeException("Unsupported MovementModifier in Main file. Fatal Error.");

        }
    }


}
