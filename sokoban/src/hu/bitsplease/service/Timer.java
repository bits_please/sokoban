package hu.bitsplease.service;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.*;

/**
 * Periodikus időzítő, ami a léptethető dolgokat lépteti.
 */
public class Timer {

    private static Timer instance = null;

    private Timer(){}

	private static List<Steppable> steppables = new ArrayList<>();
	private static boolean running = false;
	private static long tick_value = 200;

    /**
     *
     * @throws InterruptedException
     */
	private static void tick() throws InterruptedException {
	    while(running) {
            long time = java.lang.System.currentTimeMillis();
            //System.out.println("Current Time: " + time);
            long next_tick = time + tick_value;
            //System.out.println("Next Tick: " + next_tick);
            for (Steppable steppable : steppables) {
                steppable.step();
            }
            if(next_tick > java.lang.System.currentTimeMillis()){
                long wait_for_miliseconds = next_tick - java.lang.System.currentTimeMillis();
                if(wait_for_miliseconds < 0){
                    wait_for_miliseconds = 0;
                }
                //System.out.println("I Need to wait: " + wait_for_miliseconds + "because current time: " + java.lang.System.currentTimeMillis() + "and next tick is at: " + next_tick);
                Thread.sleep(wait_for_miliseconds);
            }
        }
	}

	public static void Pause(){
	    running = false;
    }

    public static void Start(){
	    if(instance == null){
	        instance = new Timer();
        }
	    running = true;
        try {
            tick();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

	public static void addSteppable(Steppable s) {
		steppables.add(s);
	}

	public static void removeSteppable(Steppable s) throws NotFound {
		boolean value = steppables.remove(s);
		if(!value){
			throw new NotFound();
		}
	}

}
