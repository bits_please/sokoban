package hu.bitsplease.service;


import java.rmi.UnexpectedException;

/**
 * Enumeráció, irányt jelöl.
 */
public enum Direction {


	LEFT,
	RIGHT,
	UP,
	DOWN,
	NONE;

	public static Direction opposite(Direction d){
		switch(d){
			case DOWN:
				return UP;
			case UP:
				return DOWN;
			case LEFT:
				return RIGHT;
			case RIGHT:
				return LEFT;
			default:
				throw new RuntimeException("What in the name of all that's holy can cause this exception? Seriously wtf?!?");
		}
	}


}
