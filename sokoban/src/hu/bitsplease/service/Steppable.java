package hu.bitsplease.service;

import java.io.*;
import java.util.*;

public interface Steppable {

	void step();

}
