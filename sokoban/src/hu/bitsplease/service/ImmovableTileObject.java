package hu.bitsplease.service;

import java.io.*;
import java.util.*;

public abstract class ImmovableTileObject extends TileObject {

	public abstract void movableArrive(MovableTileObject movableObject);

	public abstract void movableArrive(MovableTileObject movableObject, MovableTileObject pusher);

	public abstract void movableLeave(MovableTileObject movableObject);

	public abstract void movableLeave(MovableTileObject movableObject, MovableTileObject pusher);
}
