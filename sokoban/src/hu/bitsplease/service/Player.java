package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * A Workerek mozgatásáért felel. A felhasználók ezen keresztül képesek irányítani munkásaikat.
 */
public class Player implements Steppable {

	/**
	 * Az irányított munkást tárolja.
	 */
	private Worker worker = new Worker();

	/**
	 * A játékos pontjainak száma.
	 */
	private int points;

	/**
	 * A játékos neve
	 */
	private String name;

	/**
	 * A megadott irányba megpróbálja mozgatni a munkást.
	 * @param nextDirection a megadott irány
	 */
	public void moveWorker(Direction nextDirection) {
//		Util.printTab();
//		Util.printCall("worker", "Worker", "tryToMove", "direction");
		worker.tryToMove(nextDirection);	//Timer step() előtt történi mozgási kísérlet

//		Util.printTab();
//		Util.printCall("w", "Worker", "step", "");
		worker.step();	//szimuláljuk a Timer működését

//		Util.printReturn("p", "Player", "moveWorker", "direction");
	}

	public Worker getWorker() {
		return worker;
	}

	public void setWorker(Worker worker) {
		this.worker = worker;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void step() {

	}
}
