package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Erre kell rátolni egy ládát. Ha rátolnak egy ládát, pontot ad a rátoló játékosnak és a láda helyére oszlopot rak.
 */
public class CrateDestination extends ImmovableTileObject {

	/**
	 * A rajta lévő ládát destroy()-olja, majd a saját Tile-ján lecseréli magát egy Wall-ra.
	 */
	private void turnIntoWall() {
		this.getTile().getMovableObject().destroy();

		Wall w = new Wall();

		this.getTile().setImmovableObject(w);
	}

	/**
	 * Érzékeli, ha rálép valaki magától, egyelőre semmit sem csinál ekkor
	 * @param movableObject rálépő objektum
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject) {
		int w = movableObject.getWeight();
		if(w > 5 && w <= 10){
			turnIntoWall();
		}
	}

	/**
	 * Érzékeli, ha megérkezik egy láda, pontot ad a rátoló játékosnak
	 * (a a játékos több ládát is tolt, akkor megkeresi a játékost).
	 * Meghívja a saját turnToWall()-ját.
	 * @param movableObject
	 * @param pusher
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject, MovableTileObject pusher) {
		/*Util.printTab();
		Util.printCall("movableObject", "MovableTileObject", "getWeight", "");

		int w = movableObject.getWeight();
		Util.printTab();

		Util.printCall("movableObject", "MovableTileObject", "getPusher", "");

		pusher = movableObject.getPusher();
		Worker wo = (Worker) pusher;

		Util.printTab();
		Util.printCall("pusher", "Worker", "addPoint", "");

		wo.addPoint();

		Util.printReturn("c", "CrateDestionation", "movableArrive", "crate, worker");*/

		int weight = movableObject.getWeight();
		int w;

		if(weight > 5){
			do{
				pusher = movableObject.getPusher();
				w = pusher.getWeight();
			}while (w < 5 && pusher == null);

			if(w > 0 && w <= 5){
				((Worker) pusher).addPoint();
			}

			turnIntoWall();
		}

	}

	@Override
	public void movableLeave(MovableTileObject movableObject) {

	}

	@Override
	public void movableLeave(MovableTileObject movableObject, MovableTileObject pusher) {

	}

	/**
	 * Igaz értékkel tér vissza. Ha olyan TileObject-et akarunk, ami képes akadályozni (pl ilyen a Wall),
	 * akkor ezt override-olni kell.
	 * @param movableObject ezt vizsgáljuk, hogy léphetünk-e rá
	 * @return true, ha igen
	 */
	@Override
	public boolean canStepOnThis(MovableTileObject movableObject) {
		return true;
	}

	/**
	 * Bővítés célából van, egyelőre nem bír funkcióval.
	 */
	@Override
	public void step() {

	}
}
