package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Minden olyan objektum, ami mezőn tartózkodik.
 */
public abstract class TileObject implements Steppable {

	protected Tile tile;


	public abstract boolean canStepOnThis(MovableTileObject movableObject);

	public Tile getTile() {
//		Util.printTab();
//		Util.printTab();
//		Util.printReturn("worker", "Worker", "getTile", "direction");
		return tile;
	}

	public void setTile(Tile tile) {
		this.tile = tile;
	}
}
