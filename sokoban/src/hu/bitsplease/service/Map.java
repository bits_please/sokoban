package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * A jelenlegi játékot reprezentáló osztály.
 */
public class Map {
	/**
	 * The number of players to play on the map
	 */
	int playerNumber;
	/**
	 * The name of the map
	 */
	String name;


	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	/**
	 * A játékosokat tartalmazó List.
	 */
	private List<Player> players;
	/**
	 * A mezőket tartalmazó List.
	 */
	private List<Tile> tiles;

	public Map(String name, List<Tile> tiles, int playerNumber){
		this.name = name;
		this.tiles = tiles;
		this.playerNumber = playerNumber;
		this.players = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}






	/**
	 * Megállítja a jelenlegi játékot.
	 */
	public void stopGame() {
		Game.gameEnds(players);
		players = null;
	}


	/**
	 * Hozzáadja a játékosok neveit a listához.
	 * @param names a játékosok nevei
	 */
	public void addPlayers(List<String> names) {
		for(String n : names){
			Player p = new Player();
			for(Tile t : tiles){
				if(t.getMovableObject() != null && t.getMovableObject() instanceof Worker && ((Worker) t.getMovableObject()).getPlayer() == null){
					p.setWorker((Worker)t.getMovableObject());
					((Worker) t.getMovableObject()).setPlayer(p);
					break;
				}
			}
			p.setName(n);
			players.add(p);
		}
	}

	public List<Player> getPlayers() {
		return players;
	}

	public List<Tile> getTiles() {
		return tiles;
	}



}
