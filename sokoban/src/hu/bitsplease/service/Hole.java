package hu.bitsplease.service;

import java.io.*;
import java.util.*;

/**
 * Egy lyukat reprezentál, ami a rákerülő objektumot megsemmisíti, legyen az láda vagy munkás.
 */
public class Hole extends ImmovableTileObject {

	/**
	 * A rákerülő mozgatható objektumot megsemmisíti azzal, hogy meghívja a MovableTileObject destroy() metódusát.
	 * @param m megsemmisíti ezt az objektumot
	 */
	public void destroyMovable(MovableTileObject m) {
		m.destroy();
		//Itt még lehet kéne csinálni mást passz
	}

	/**
	 * Meghívja a destroyMovable() metódust movableObject-re, amennyiben ő egy láda.
	 * @param movableObject erre hívja meg a destroyMovable() metódust
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject) {
		destroyMovable(movableObject);
	}

	/**
	 * Meghívja a movableArrive(MovableTileObject m) metódust movableObject-re.
	 * @param movableObject az érkezett objektum
	 * @param pusher toló objektum
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject, MovableTileObject pusher) {
		destroyMovable(movableObject);
		//Rekurzió miatt itt lehet el fog szállni!!!!, de elvileg am nem
	}

	@Override
	public void movableLeave(MovableTileObject movableObject) {

	}

	@Override
	public void movableLeave(MovableTileObject movableObject, MovableTileObject pusher) {

	}

	/**
	 * Igaz értékkel tér vissza, ha olyan TileObject-et akarunk, ami képes akadályozni (pl ilyen a Wall),
	 * akkor ezt override-olni kell.
	 * @param movableObject ezt vizsgáljuk, hogy léphetünk-e rá
	 * @return true, ha igen
	 */
	@Override
	public boolean canStepOnThis(MovableTileObject movableObject) {
		return true;
	}

	@Override
	public void step() {

	}

}
