package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

/**
 * Egy kapcsolót reprezentál. Pontosan egy lyuk tartozik hozzá, melyet képes aktiválni és deaktiválni.
 */
public class Switch extends ImmovableTileObject {

	/**
	 * Megmutatja a kapcsoló állapotát: hamis, ha inaktív, igaz, ha aktív.
	 */
	private boolean switched = false;

	public void setHole(SwitchHole hole) {
		this.hole = hole;
	}

	/**
	 * A hozzá tartozó SwitchHole-t tárolja.
	 */
	private SwitchHole hole;

	/**
	 * Eldönti, hogy az adott movableObject-re kapcsolni kell -e.
	 * @param movableObject a rálépő objektum
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject) {
		switched = checkWeightOnPlate(movableObject);
	}

	/**
	 *	Megmondja hogy a súly elegendő-e a a switched állapot átkapcsolásához
	 * @param mto A rálépő Movable Tile Object
	 * @return
	 */
	private boolean checkWeightOnPlate(MovableTileObject mto){

		int w = mto.getWeight();
		if(w > 5 && w <= 10){
			return true;
		}
		return false;
	}

	/**
	 * Továbbhívja a movableArrive(movableObject)-et.
	 * @param movableObject a rálépő objektum
	 * @param pusher toló
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject, MovableTileObject pusher) {
		switched = checkWeightOnPlate(movableObject);
	}

	@Override
	public void movableLeave(MovableTileObject movableObject) {

	}

	@Override
	public void movableLeave(MovableTileObject movableObject, MovableTileObject pusher) {

	}

	@Override
	public boolean canStepOnThis(MovableTileObject movableObject) {
		return true;
	}

	@Override
	public void step() {

	}

	/**
	 * Visszatér a switched értékével.
	 * @return true, ha már átkapcsolt
	 */
	public boolean IsSwitched() {
		return switched;
	}

	public SwitchHole getHole() {
		return hole;
	}
}
