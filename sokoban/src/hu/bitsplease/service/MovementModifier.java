package hu.bitsplease.service;

public abstract class MovementModifier extends ImmovableTileObject {

    protected double powerModifier;

    @Override
    public void movableArrive(MovableTileObject movableObject) {
        movableObject.setPower(powerModifier*movableObject.getPower());
    }

    @Override
    public void movableArrive(MovableTileObject movableObject, MovableTileObject pusher) {
        movableArrive(movableObject);
    }

    @Override
    public void movableLeave(MovableTileObject movableObject) {
        movableObject.setPower(movableObject.getPower()/powerModifier);
    }

    @Override
    public void movableLeave(MovableTileObject movableObject, MovableTileObject pusher) {
        movableLeave(movableObject);
    }

    @Override
    public boolean canStepOnThis(MovableTileObject movableObject) {
        return true;
    }

}
