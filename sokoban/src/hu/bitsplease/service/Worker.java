package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Egy munkást reprezentál. Számon tartja a pontjait és hogy melyik játékos irányítja.
 * Ládákat tolhat el, illetve egy láda eltolhatja őt.
 */
public class Worker extends MovableTileObject {

	private Player player;

	public Worker(){
		weight = 5;
	}

	/**
	 * Hozzáad egyet a pontokhoz.
	 */
	public void addPoint() {
		player.setPoints(player.getPoints() + 1);
	}

	/**
	 * 2. fázis, a metódus rekurzíve meghívja a d irányban lévő MovableTileOject-re is ezt a metódust,
	 * majd ha az true, és movableObject is tolhatja a Worker-t, akkor true-val tér vissza.
	 * Ennek meg kell előznie a push() metódushívást.
	 * @param movableObject A kezdeményező objektum
	 * @param direction Az irány amely fele haladunk
	 * @return
	 */
	@Override
	protected boolean canPush(MovableTileObject movableObject, Direction direction) {
		int w = movableObject.getWeight();

		if(w > 0 && w <= 5){
			if(!this.isTheOnlyOnePushing(movableObject)){
				return false;
			}
			else{
				Tile t1 = tile.getNeighbourTile(direction);
				MovableTileObject mto = t1.getMovableObject();
				boolean canpush = mto.canPush(mto, direction);
				if(canpush){
					return true;
				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
	public boolean tryToMove(Direction where) {
		Tile t1 = tile.getNeighbourTile(where);
		List<ImmovableTileObject> imos = t1.getImmovableObjects();
		//boolean canStepOnAll = true;
		for(ImmovableTileObject ito : imos){

			if (!ito.canStepOnThis(this)) {
				//canStepOnAll = false;
				return false;
			}
		}

		MovableTileObject mto = t1.getMovableObject();
		if(mto != null){
			if(!mto.tryToMove(where)){
				return false;
			}

			if(mto.getWeight() > 5 && mto.getWeight() <= 10){
				mto.reservePush(this,where);
			}
		}


		t1.reserveMove(this);

		this.setActualMove(where);
		return true;

	}
	public void dropMovementModifier(MovementModifier m){
		this.getTile().addImmovableTileObject(m);
	}
	/**
	 * 2. fázis. Egy informált, 100%-ig biztos true vagy false értéket ad vissza, mely azt reprezentálja, hogy az adott objektum el fog -e mozdulni a mostani helyéről.
	 * Megvizsgálja, hogy ahova menni akarunk, oda tényleg tudunk -e, ezt az esetleges canPush() hívással együtt teszi, továbbá vizsgál Tile reservation-t is. (mindezt rekurzívan)
	 * @return
	 */
	@Override
	public boolean willMove() {
		if(getActualMove() != Direction.NONE){
			Tile t1 = tile.getNeighbourTile(getActualMove());
			boolean ami = t1.amITheOnlyReserved(this);
			if(!ami) return false;
			MovableTileObject mto = t1.getMovableObject();

			if(mto == null) return true;

			int w = mto.getWeight();

			if(w > 5 && w <= 10){
				boolean canpush = mto.canPush(this, getActualMove());
				if(!canpush) return false;
				else return true;
			}
			else if(w > 1 && w <= 5){
				boolean willmove = mto.willMove();

				if(willmove) return true;

				else return false;
			}
		}
		return false;
	}

	@Override
	public int getWeight() {
		return weight;
	}


	@Override
	public boolean canStepOnThis(MovableTileObject movableObject) {
		return false;
	}

	@Override
	public void step() {
		//System.out.println("Im doing my step as a wroker");
		if(actualMove != Direction.NONE) {
			if(!this.willMove()){
				this.setActualMove(Direction.NONE);
			}
			else{
				Tile t1 = tile.getNeighbourTile(actualMove);
				MovableTileObject mto = t1.getMovableObject();
				if(mto == null){
					this.move();
				}
				else{
					boolean willMove = mto.willMove();

					if(willMove){
						this.move();
					}
					else{
						mto.push(this,actualMove);
						this.move();
					}
				}
			}
		}
	}
	@Override
	public void destroy(){
		Game.getCurrentMap().getPlayers().stream().filter(p -> (p.getWorker().equals(this))).findAny().get().setWorker(null);
	}
}
