package hu.bitsplease.service;

import java.io.*;
import java.util.*;

/**
 * Falat vagy oszlopot reprezentál. A mezőre, amin található, nem mozoghat más objektum.
 */
public class Wall extends ImmovableTileObject {

    //Method cannot be called on Walls
    @Override
    public void movableArrive(MovableTileObject movableObject) {
        throw new IllegalStateException();
    }

    //Method cannot be called on Walls
    @Override
    public void movableArrive(MovableTileObject movableObject, MovableTileObject pusher) {
        throw new IllegalStateException();
    }

    //You can not step on a wall
    @Override
    public void movableLeave(MovableTileObject movableObject) {

    }

    @Override
    public void movableLeave(MovableTileObject movableObject, MovableTileObject pusher) {

    }

    @Override
    public boolean canStepOnThis(MovableTileObject movableObject) {
        return false;
    }


    @Override
    //Wall does not step
    public void step(){
        //wall does nothing
    }
}
