package hu.bitsplease.service;

import hu.bitsplease.cli.Util;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import javax.activation.UnsupportedDataTypeException;
import java.io.*;
import java.util.*;

/**
 * Ez egy singleton osztály. A játék indításáért, befejezéséért és a pályák betöltéséért felel.
 */
public class Game {
	public static Map getCurrentMap() {
		return currentMap;
	}

	public static void setCurrentMap(Map currentMap) {
		Game.currentMap = currentMap;
	}

	/**
	 * A kiválasztott pályát tárolja
	 */
	private static Map currentMap;

	public static List<Map> getMaps() {
		return maps;
	}

	/**
	 * Az összes betöltött pályát tárolja.
	 */
	private static List<Map> maps = new ArrayList<>();
	private static Game game;
	private static Thread timerThread;
	public static List<Player> getLastGameResults() {
		if(!isStarted && lastGameResults != null){
			return lastGameResults;
		}
		throw new IllegalStateException("Game results queried with a game still running or without any results");

	}

	private static List<Player> lastGameResults;
	private static List<String> players = new ArrayList<>();

	public static boolean isIsStarted() {
		return isStarted;
	}

	public static void setIsStarted(boolean isStarted) {
		Game.isStarted = isStarted;
	}

	private static boolean isStarted = false;
	private Game() throws IOException{
		loadMaps();
		isStarted = false;
	}

	public static void addPlayer(String player){
		players.add(player);
	}

	public static void dropMovementModifier(String player, MovementModifier what){
		currentMap.getPlayers().stream().filter(p -> (p.getName().equals(player))).findAny().get().getWorker().dropMovementModifier(what);
	}

	/**
	 * Meghívja az initializePlayers()-t, majd az adott Map-en elindít egy játékot az adott játékosokkal. Ez előtt elvárt
	 * az addPlayer és a chooseMap meghívása.
	 *
	 */
	public static void startGame() throws IOException{
		if(game == null){
			game = new Game();
		}
		if(currentMap == null){
			throw new IllegalStateException("No map choosed!");
		}
		List<String> players = initializePlayers();
		currentMap.addPlayers(players);
		for(Tile act : currentMap.getTiles()){
			for(ImmovableTileObject ito : act.getImmovableObjects()){
				Timer.addSteppable(ito);
			}
			if(act.getMovableObject() != null){
				Timer.addSteppable(act.getMovableObject());
			}
		}
		timerThread = new Thread(){
			@Override
			public void run(){
				Timer.Start();
			}
		};
		isStarted = true;
	}

	/**
	 * Kilép a játékból.
	 */
	public static void endGame() {
		game = null;
		maps = null;
		players = null;
		isStarted = false;
	}

	static void gameEnds(List<Player> results){
		if(!isStarted){
			throw new IllegalStateException("Tried to move player before the game has started!");
		}
		timerThread.interrupt();
		timerThread = null;
		isStarted = false;
		lastGameResults = results;
	}
	public static void playerMove(String playerName, Direction d){
		if(!isStarted){
			throw new IllegalStateException("Tried to move player before the game has started!");
		}
		currentMap.getPlayers().stream().filter(p -> (p.getName().equals(playerName))).findFirst().get().moveWorker(d);
	}
	/**
	 * A pályaválasztásra ad lehetőséget.
	 */
	public static void chooseMap(String mapName) {
		currentMap = maps.stream().filter(m -> (m.name.equals(mapName))).findFirst().get();
	}

	/**
	 * Betölti a pályákat az alapértelmezett elérési útvonalról,
	 * ténylegesen eltárolni csak azokat a pályákat engedi, amik teljesen valid-ak.
	 */
	public static void loadMaps()throws IOException{
		File directory = new File("maps");
		for(File f : directory.listFiles()){
			loadMap(f);
		}
	}
	public static void loadMap(File f) throws IOException{
		SAXBuilder builder = new SAXBuilder();
		try {
			Document d = builder.build(f);
			Element root = d.getRootElement();
			List<Element> children = root.getChildren();
			java.util.Map<Integer, Tile> tileMap = new HashMap<>();
			int playerNumber = 0;
			if(root.getName().equals("tiles")){
				//Read every element, parse it as a Tile, then parse its immovable and movable tile objects
				for(Element e : children){
					int id = Integer.parseInt(e.getChildText("id"));
					tileMap.put(id, new Tile());
				}

				for(Element e : children){
					int id = Integer.parseInt(e.getChildText("id"));
					if(e.getChild("immovableobject") != null){
						String name = e.getChild("immovableobject").getChildText("name");
						ImmovableTileObject ito = immovableTileObjectFactory(name);
						if(ito instanceof Switch){
							int id2 = Integer.parseInt(e.getChild("immovableobject").getChildText("switchHole"));
							if(null == tileMap.get(id2).getImmovableObjects() || tileMap.get(id2).getImmovableObjects().stream().noneMatch(s -> (s instanceof SwitchHole))){
								((Switch) ito).setHole(new SwitchHole());
							}else{
								for(ImmovableTileObject ito2 : tileMap.get(id2).getImmovableObjects()){
									if(ito2 instanceof SwitchHole){
										ito = ((SwitchHole) ito2).getS();
										((Switch) ito).setHole((SwitchHole)ito2);
									}
								}
							}
						}else if(ito instanceof SwitchHole){
							int id2 = Integer.parseInt(e.getChild("immovableobject").getChildText("switch"));
							if(null == tileMap.get(id2).getImmovableObjects() || tileMap.get(id2).getImmovableObjects().stream().noneMatch(s -> (s instanceof Switch))){
								((SwitchHole) ito).setS(new Switch());
							}else{
								for(ImmovableTileObject ito2 : tileMap.get(id2).getImmovableObjects()){
									if(ito2 instanceof Switch){
										ito = ((Switch) ito2).getHole();
										((SwitchHole) ito).setS((Switch)ito2);
									}
								}
							}
						}
						tileMap.get(id).addImmovableTileObject(ito);
						ito.setTile(tileMap.get(id));
					}

					if(e.getChild("movableobject") != null) {
						String name = e.getChild("movableobject").getChildText("name");
						MovableTileObject m = movableTileObjectFactory(name);
						m.setWeight(Integer.parseInt(e.getChild("movableobject").getChildText("weight")));
						m.setPower(Double.parseDouble(e.getChild("movableobject").getChildText("power")));
						if(m.getWeight() >= 1 && m.getWeight() <= 5){
							playerNumber++;
						}
						tileMap.get(id).addMovableTileObject(m);
						m.setTile(tileMap.get(id));
					}
				}

				//Now discover and implement the neighbrouship relations between the Tiles. This is only possible, because they are
				//stored in the tileList by the id they were read in
				for(Element e : children){
					//get every <neighbour>
					List<Element> neighbours = e.getChildren("neighbour");
					for(Element n : neighbours){

						String directionName = n.getAttribute("direction").getValue();
						Direction dir = Direction.valueOf(directionName);

						int id = Integer.parseInt(e.getChildText("id")); //The id of the actual Tile
						int neighbourId = Integer.parseInt(n.getChildText("id")); //The id of the neighbouring Tile
						//Get the actual Tile form the list, then find the Tile with the neighbour id, and add each other to each others neighbours
						Tile act = tileMap.get(id);
						Tile neigh = tileMap.get(neighbourId);
						if((act.getNeighbours().containsKey(dir) && !act.getNeighbours().get(dir).equals(neigh))
								|| (neigh.getNeighbours().containsKey(Direction.opposite(dir)) && !neigh.getNeighbours().get(Direction.opposite(dir)).equals(act))){
							throw new IOException("Tried to set two neighbours on the same Tile with the same Direction! Abort!!");
						}else if(!act.getNeighbours().containsKey(dir)){
							act.setNeighbour(dir, neigh);
							if(!neigh.getNeighbours().containsKey(dir)){
								neigh.setNeighbour(Direction.opposite(dir), act);
							}
						}
					}
				}
			}
			List<Tile> tileList = new ArrayList<>();
			tileList.addAll(tileMap.values());
			maps.add(new Map(root.getAttributeValue("name"), tileList, playerNumber));
		}catch(IOException e){
			throw e;
		}catch(JDOMException e){
			throw new IOException(e);
		}
	}


	/**
	 * Returns a ImmovableTileObject based on a String, used for xml deserialisation. If no such ImmovableTileObject exists,
	 * then it throws an UnsupportedDataTypeException.
	 * @param name the name of the ImmovableTileObject
	 * @return the ImmovableTileObject
	 * @throws UnsupportedDataTypeException
	 */
	private static ImmovableTileObject immovableTileObjectFactory(String name) throws UnsupportedDataTypeException{
		name.toLowerCase();
		switch(name){
			case "\"hole\"":
				return new Hole();
			case "\"wall\"":
				return new Wall();
			case "\"crateDestination\"":
				return new CrateDestination();
			default:
				if(name.startsWith("\"switchHole\"")){
					return new SwitchHole();
				}else if(name.startsWith("\"switch\"")){
					return new Switch();
				}
				throw new UnsupportedDataTypeException("Unsupported ImmovableTileObject: " + name + " in map file. Fatal Error.");
		}
	}

	/**
	 * Returns a MovableTileObject based on a String, used for xml deserialisation. If no such MovableTileObject exists,
	 * then it throws an UnsupportedDataTypeException.
	 * @param name the name of the MovableTileObject
	 * @return the MovableTileObject
	 * @throws UnsupportedDataTypeException
	 */
	private static MovableTileObject movableTileObjectFactory(String name) throws UnsupportedDataTypeException{
		name.toLowerCase();
		switch(name){
			case "\"worker\"":
				return new Worker();
			case "\"crate\"":
				return new Crate();
			default:
				throw new UnsupportedDataTypeException("Unsupported MovableTileObject" + name + " in map file. Fatal Error.");
		}
	}

	/**
	 * If players is empty, return default player names.
	 * @return Returns players, or if players is empty, returns players with default names
	 */
	private static List<String> initializePlayers() {
		if(players.isEmpty()){
			for(int i = 0; i < currentMap.getPlayerNumber(); i++){
				players.add("Player " + i);
			}
		}else if(!(players.size() == currentMap.getPlayerNumber())){
			throw new IllegalStateException("The number of players does not match the number of players the map should have!");
		}
		return players;
	}
	public static void iDontKnowWhyWeNeedThisButWithThisItWorks(){}


}
