package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Egy kapcsolóhoz tartozó lyukat reprezentál. Lehet aktiválni és deaktiválni.
 */
public class SwitchHole extends Hole {

	public Switch getS() {
		return s;
	}

	public void setS(Switch s) {
		this.s = s;
	}

	/**
	 * Tárolja a hozzátartozó kapcsolót.
	 */
	private Switch s;

	/**
	 * A switch adattag vizsgálata után semmit sem csinál, vagy destroy()-olja m-et.
	 * @param movableObject erre hívja meg a destroyMovable() metódust
	 */
	@Override
	public void movableArrive(MovableTileObject movableObject) {
		if(s.IsSwitched()){
			movableObject.destroy();
		}
	}

}
