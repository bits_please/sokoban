package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Egy ládát reprezentál. Képes egyik mezőről a másikra menni.
 * Eltolhat egy munkást, illetve egy munkás eltolhatja őt. Képes megölni egy munkást.
 */
public class Crate extends MovableTileObject {

	/**
	 * Megöli a megadott objektumot (destroy())
	 * @param m a megadott objektum
	 */
	private void crush(MovableTileObject m) {
		m.destroy();
	}

	/**
	 * Rekurzíve végighívódik a tolni akarókon, visszatér true-val, ha movableObject objektum d irányba képes őt mozgatni.
	 * @param movableObject toló
	 * @param direction irány
	 * @return true, ha képes mozgatni
	 */
	@Override
	protected boolean canPush(MovableTileObject movableObject, Direction direction) {
		int w = movableObject.getWeight();

		if(w >= 1 && w <= 10){
			if(!this.isTheOnlyOnePushing(movableObject)){
				return false;
			}
			else{
				Tile t1 = tile.getNeighbourTile(direction);
				MovableTileObject mto = t1.getMovableObject();
				if(mto != null){
					boolean canpush = mto.canPush(mto, direction);
					if(canpush){
						return true;
					}
					else {
						return false;
					}
				}

			}
		}
		return true;
	}

	/**
	 * Megnézi, hogy az adott objektum tud -e mozogni az adott irányba.
	 * Amennyiben egy másik MovableTileObject is van ott, annak meghívja a tryToMove-ját.
	 * Ha a mozgás lánc lehetséges, ugyan ezen a láncon végighívja a reservePush()-ot is, a Tile-re pedig a reserveMove()-ot.
	 * @param where irány
	 * @return true, ha mozoghat
	 */
		@Override
		public boolean tryToMove(Direction where) {
			Tile t1 = tile.getNeighbourTile(where);
			List<ImmovableTileObject> imos = t1.getImmovableObjects();
			//boolean canStepOnAll = true;
			for(ImmovableTileObject ito : imos){

				if (!ito.canStepOnThis(this)) {
					//canStepOnAll = false;
					return false;
				}
			}

			MovableTileObject mto = t1.getMovableObject();
			if(mto != null){
				int w = mto.getWeight();

				if((w > 0 && w <= 5) && !mto.tryToMove(where)){
					return false;
				}
				else if(w > 5 && w <= 10) {
					mto.reservePush(this, where);
					t1.reserveMove(this);
					setActualMove(where);
					return true;
				}
			}

		return true;
	}

	/**
	 * Csak és kizárólag a step()-ből meghívandó metódus.
	 * Rekurzívan végigvizsgálja a mozgás helyén levő MovableTileObject-eket.
	 * @return visszatérése true, ha ez az objektum ebben a step()-ben mozogni fog
	 */
	@Override
	public boolean willMove() {

		if(getActualMove() != Direction.NONE){
			Tile t1 = tile.getNeighbourTile(getActualMove());
			boolean ami = t1.amITheOnlyReserved(this);
			if(!ami) return false;
			MovableTileObject mto = t1.getMovableObject();

			if(mto == null) return true;

			int w = mto.getWeight();

			if(w > 5 && w <= 10){
				boolean canpush = mto.canPush(this, getActualMove());
				if(!canpush) return false;
				else return true;
			}
			else if(w > 1 && w <= 5){
				 boolean willmove = mto.willMove();
				 if(willmove) return true;

				 else{
				 	boolean canpush = mto.canPush(this, getActualMove());
				 	if(!canpush) return false;
				 	else return true;
				 }
			}
		}
		return false;
	} //Ebben a peldaban mindig tovabb lehet tolni a ladat

	/**
	 *  Igaz értékkel tér vissza, ha olyan TileObject-et akarunk, ami képes akadályozni (pl ilyen a Wall),
	 *  akkor ezt override-olni kell.
	 * @param movableObject ezt vizsgáljuk, hogy léphetünk-e rá
	 * @return true, ha igen
	 */
	@Override
	public boolean canStepOnThis(MovableTileObject movableObject) {
		return false;
	}

	/**
	 * Bővítés célából van, egyelőre nem bír funkcióval
	 */
	@Override
	public void step() {
		//System.out.println("Im doing my best as a crate");
	}

	/**
	 * A súly gettere.
	 * @return visszatér 10-el
	 */
	public int getWeight(){
		/*Util.printTab();
		Util.printTab();
		Util.printReturn("mto", "MovableTileObject", "getWeight", "");*/
		return 10;
	}
}
