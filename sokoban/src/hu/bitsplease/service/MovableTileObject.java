package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import javax.management.ImmutableDescriptor;
import java.io.*;
import java.util.*;
import java.util.Map;

public abstract class MovableTileObject extends TileObject {
	protected int weight;
	protected double power;
	protected java.util.Map<MovableTileObject, Direction> reservedPushes = new HashMap<>();
	protected Direction actualMove = Direction.NONE;

	protected void move() {


		//Util.printTab();
		//Util.printTab();
		//Util.printReturn("mto", "MovableTileObject", "move", "");

		//If null detect the error
		if(actualMove == null){
			throw new IllegalArgumentException();
		}

		Tile destination = tile.getNeighbourTile(actualMove);
		//Removes itself from the current tile
		tile.removeMovableTileObject(this);

		//Here i replace the neighbour movable tile object to this one
		MovableTileObject mto = destination.getMovableObject();
		if(mto != null){
			destination.removeMovableTileObject(mto);
		}
		destination.addMovableTileObject(this);
		this.tile = destination;
		//Moving and notifying the Immovable tile objects on the neighbouring tile
		List<ImmovableTileObject> immovableTileObjects = destination.getImmovableObjects();
		for(ImmovableTileObject object : immovableTileObjects){
			if(getPusher() != null){
				object.movableArrive(this, getPusher());
			}
			else{
				object.movableArrive(this);
			}
		}
	}



	public void setWeight(int weight) {
		this.weight = weight;
	}

	public void setReservedPushes(Map<MovableTileObject, Direction> reservedPushes) {
		this.reservedPushes = reservedPushes;
	}

	public void setActualMove(Direction actualMove) {
		this.actualMove = actualMove;
	}

	public void destroy() {
		this.tile.setMovableObject(null);
	}

	protected abstract boolean canPush(MovableTileObject movableObject, Direction direction);

	protected void reservePush(MovableTileObject who, Direction where) {
		this.reservedPushes.put(who, where);
	}

	protected void push(MovableTileObject who, Direction where){
		//Util.printTab();
		//Util.printTab();
		//Util.printReturn("mto", "MovableTileObject", "push", "who, where");
		if(where != null){
			Tile neighbour = tile.getNeighbourTile(where);
			MovableTileObject mto = neighbour.getMovableObject();
			if(mto != null){

				int weight = mto.getWeight();

				if(weight > 0 && weight <= 5){
					mto.push(this, where);
				}
				else if(weight > 5 && weight <= 10){
					if(mto.getWeight() > 0 && mto.getWeight() <= 5){
						Tile neighbour2 = neighbour.getNeighbourTile(where);
						List<ImmovableTileObject> itos = neighbour2.getImmovableObjects();
						for(ImmovableTileObject ito : itos){
							if (!ito.canStepOnThis(mto)) {
								mto.destroy();
								break;
							}
						}
					}else{
						mto.push(this, where);
					}

				}
			}
			if(this.actualMove != where){
				this.actualMove = where;
			}
			this.move();
		}
	}

	public abstract boolean tryToMove(Direction where);

	public abstract boolean willMove();

	public boolean isTheOnlyOnePushing(MovableTileObject movableObject) {
		if(reservedPushes.size() == 1){
			if(reservedPushes.keySet().contains(movableObject)){
				return true;
			}
		}
		return false;
	}

	public MovableTileObject getPusher() {
		//Util.printTab();
		//Util.printReturn("movableObject", "MovableTileObject", "getPusher", "");
		if(reservedPushes.size() == 1){
			return reservedPushes.keySet().iterator().next();
		}
		else if (reservedPushes.size() > 1){
			throw new IllegalStateException("Wtf how?!?");
		}else{
			return null;
		}
	}

	public abstract int getWeight();

	public Map<MovableTileObject, Direction> getReservedPushes() {
		return reservedPushes;
	}

	public Direction getActualMove() {
		return actualMove;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}
}
