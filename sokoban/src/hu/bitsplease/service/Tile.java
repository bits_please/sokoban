package hu.bitsplease.service;

import hu.bitsplease.cli.Util;

import java.io.*;
import java.util.*;

/**
 * Egy mezőt reprezentál. Nyilvántartja szomszédjait, és a rajta tartózkodó összes objektumot.
 */
public class Tile {

	private EnumMap<Direction, Tile> neighbours;
	private List<ImmovableTileObject>  immovableObjects = new ArrayList<>();
	private MovableTileObject movableObject;
	private Set<MovableTileObject> reservedObjects;


	public Tile(){
		this.neighbours = new EnumMap<>(Direction.class);
		this.reservedObjects = new HashSet<>();


	}
	public EnumMap<Direction, Tile> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(EnumMap<Direction, Tile> neighbours) {
		this.neighbours = neighbours;
	}

	public List<ImmovableTileObject> getImmovableObjects() {
		//Util.printReturn("t2", "Tile", "getImmovableObject", "");
		return immovableObjects;
	}
	public void setNeighbour(Direction d, Tile t){
		this.neighbours.put(d,t);
	}

	public void setImmovableObject(ImmovableTileObject immovableObject) {
		immovableObjects.add(immovableObject);
	}

	public MovableTileObject getMovableObject() {
		//Util.printTab();
		//Util.printTab();
		//Util.printReturn("t2", "MovableTileObject", "getMovableObject", "");
		return movableObject;
	}

	public void setMovableObject(MovableTileObject movableObject) {
		this.movableObject = movableObject;
	}

	public Set<MovableTileObject> getReservedObjects() {
		return reservedObjects;
	}

	public void setReservedObjects(Set<MovableTileObject> reservedObjects) {
		this.reservedObjects = reservedObjects;
	}


	public void addMovableTileObject(MovableTileObject t) {
		movableObject = t;
	}

	public void addImmovableTileObject(ImmovableTileObject t) {
		immovableObjects.add(t);
	}

	public void removeMovableTileObject(MovableTileObject t) {
		if(t.equals(movableObject)){
			movableObject = null;
		}
	}

	public void removeImmovableTileObject(ImmovableTileObject t) {
		if(immovableObjects.contains(t)){
			immovableObjects.remove(immovableObjects.indexOf(t));
		}
	}

	public Tile getNeighbourTile(Direction which) {
//		Util.printTab();
//		Util.printTab();
//		Util.printReturn("t1", "Tile", "getNeighbourTile", "direction");
		return neighbours.get(which);
	}

	public Tile(EnumMap<Direction, Tile> p_neighbours) {
		neighbours = p_neighbours;
	}

	public void reserveMove(MovableTileObject m) {
		this.reservedObjects.add(m);
	}

	/**
	 * Megadja, hogy m -e az egyetlen, aki az előző step() óta rezervelni akart rá
	 * @param m
	 * @return
	 */
	public boolean amITheOnlyReserved(MovableTileObject m) {
		//Util.printTab();
		//Util.printTab();
		//Util.printReturn("t2", "Tile", "amITheOnlyOneReserved", "worker");
		if(reservedObjects.size() == 1 && reservedObjects.contains(m)){
			return true;
		}
		return false;
	}

}
