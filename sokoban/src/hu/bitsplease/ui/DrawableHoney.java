package hu.bitsplease.ui;

import hu.bitsplease.service.Honey;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * A méz kirajzolására szolgál
 */
public class DrawableHoney extends DrawableMovementModifier {
    /**
     * A hozzá tartozó méz
     */
    protected Honey honey;

    /**
     * Konstruktor
     * @param m_honey méz
     */
    DrawableHoney(Honey m_honey){
        img = new ImageIcon("img\\Honey.png");
        honey = m_honey;
    }

    /**
     * Param nélküli konstruktor
     * @throws InvalidClassException
     */
    DrawableHoney() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    @Override
    public void draw() {
        this.setIcon(img);
        this.setVisible(true);
    }

    @Override
    public int getDepth() {
        return 2;
    }
}
