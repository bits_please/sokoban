package hu.bitsplease.ui;


import hu.bitsplease.service.Game;

import javax.swing.*;
import java.awt.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Menu extends JPanel{

    public Menu(){
        super();

        JLabel background = new JLabel();
        background.setIcon(new ImageIcon("img\\title_screen.png"));
        background.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        JButton startGameButton = new JButton("Start Game");
        c.gridx = 0;
        c.gridy = 0;
        background.add(startGameButton, c);

        JButton exitButton = new JButton("Exit");
        c.gridx = 0;
        c.gridy = 1;
        background.add(exitButton, c);
        try{
            Game.loadMaps();
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Fatal error while loading the maps!", "ERROR! VERY BAD!", JOptionPane.ERROR_MESSAGE);
        }

        java.util.List<String> mapNames = Game.getMaps().stream().map(hu.bitsplease.service.Map::getName).collect(Collectors.toList());

        JList mapList = new JList(mapNames.toArray());
        mapList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mapList.setLayoutOrientation(JList.VERTICAL);

        c.gridy = 2;
        background.add(new JLabel("Maps:"), c);


        JScrollPane listScroller = new JScrollPane(mapList);
        listScroller.setPreferredSize(new Dimension(200, 80));
        listScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        listScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        c.gridx = 0;
        c.gridy = 3;
        c.gridheight = 2;
        background.add(listScroller, c);
        this.add(background);
    }

    public void exit(){
        MainFrame.exit();

    }

}
