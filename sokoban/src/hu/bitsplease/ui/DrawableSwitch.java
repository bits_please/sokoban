package hu.bitsplease.ui;

import hu.bitsplease.service.Switch;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * A kapcsoló kirajzolására szolgáló osztály
 */
public class DrawableSwitch extends DrawableImmovableTileObject{

    /**
     * a bekapcsolt állapot képe
     */
    ImageIcon img_switched;

    /**
     * a megfelelő kapcsoló tárolása
     */
    protected Switch p_switch;

    /**
     * Konstruktor, paraméteres
     * @param m_switch
     */
    DrawableSwitch(Switch m_switch){
        img = new ImageIcon("img\\Switched_Off.png");
        img_switched = new ImageIcon("img\\Switched_On.png");
        p_switch = m_switch;
    }

    /**
     * paraméter nélküli kons
     * @throws InvalidClassException
     */
    DrawableSwitch() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    @Override
    public void draw() {
        if(p_switch.IsSwitched()){
            this.setIcon(img);
        }
        else{
            this.setIcon(img_switched);
        }
        this.setVisible(true);
    }

    @Override
    public int getDepth() {
        return 1;
    }
}
