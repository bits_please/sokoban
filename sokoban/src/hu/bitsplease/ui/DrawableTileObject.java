package hu.bitsplease.ui;
import javax.swing.*;

import hu.bitsplease.service.*;
import java.awt.*;

/**
 * absztrakt osztály a tileobjectek-nek
 */
public abstract class DrawableTileObject extends JLabel implements Drawable {
    protected ImageIcon img;
}
