package hu.bitsplease.ui;

import hu.bitsplease.service.Oil;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * Az olaj kirajzolásához használt osztály
 */
public class DrawableOil extends DrawableMovementModifier {
    /**
     * A hozzá tartozó olaj
     */
    protected Oil oil;

    /**
     * paraméteres konstrktor
     * @param m_oil
     */
    DrawableOil(Oil m_oil){
        img = new ImageIcon("img\\Oil.png");
        oil = m_oil;
    }

    /**
     * paraméter nélküli konstruktor
     * @throws InvalidClassException
     */
    DrawableOil() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    @Override
    public void draw() {
        this.setIcon(img);
        this.setVisible(true);
    }

    @Override
    public int getDepth() {
        return 2;
    }
}
