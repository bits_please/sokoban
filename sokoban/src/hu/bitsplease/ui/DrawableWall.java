package hu.bitsplease.ui;

import hu.bitsplease.service.Wall;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * A kirajzolandó falak osztálya
 */
public class DrawableWall extends DrawableImmovableTileObject{

    /**
     * a hozzátartozó fal
     */
    protected Wall wall;

    /**
     * paraméteres konstruktor, kiválasztja a megfelelő képet
     * @param m_wall
     */
    DrawableWall(Wall m_wall){
        img = new ImageIcon("img\\Wall.png");
        wall = m_wall;
    }

    DrawableWall() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    /**
     * beállítja a kiválasztott képet
     */
    @Override
    public void draw() {
        this.setIcon(img);
        this.setVisible(true);
    }

    /**
     * visszatér a mélységgel
     * @return mélység
     */
    @Override
    public int getDepth() {
        return 1;
    }
}
