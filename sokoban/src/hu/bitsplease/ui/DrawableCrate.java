package hu.bitsplease.ui;

import hu.bitsplease.service.Crate;

import javax.swing.*;
import java.awt.*;
import java.io.InvalidClassException;

/**
 * A láda kirajzolására szolgáló osztály
 */
public class DrawableCrate extends DrawableMovableTileObject{

    /**
     * A hozzá tartozó láda
     */
    protected Crate crate;

    /**
     * Paraméteres konstruktor
     * @param m_crate a megfelelő láda
     */
    DrawableCrate(Crate m_crate){
        img = new ImageIcon("img\\Crate.png");
        crate = m_crate;
    }

    /**
     * Konstruktor
     * @throws InvalidClassException
     */
    DrawableCrate() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    /**
     * kép beállítása
     */
    @Override
    public void draw() {
        this.setIcon(img);
        this.setVisible(true);
    }

    /**
     * Mélységgel tér vissza
     * @return mélység
     */
    @Override
    public int getDepth() {
        return 3;
    }
}
