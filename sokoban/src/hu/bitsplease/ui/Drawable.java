package hu.bitsplease.ui;

/**
 * Közös intefész a kirajzolandó Objecteknek
 */
public interface Drawable {
    /**
     * Kirajzolás
     */
    public void draw();

    /**
     * Mélység gettere
     * @return mélység
     */
    public int getDepth();
}
