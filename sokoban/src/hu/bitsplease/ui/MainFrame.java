package hu.bitsplease.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame {
    private static Menu menu;
    private static GameMapView currentGame;

    static boolean isExit = false;

    private MainFrame(){
        super("Sokoban");

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(1280, 720));    //Default resolution
        this.setResizable(false);       //Window can't be resized
        menu = new Menu();
        this.add(menu);
        this.pack();
        this.setLocationRelativeTo(null);   //Open in the middle of the screen
        this.setVisible(true);
    }

    public static void startGame(String mapName){
        currentGame = new GameMapView(mapName);
    }
    public static void exit(){
        isExit = true;
    }
    public static void main(String[] args){
        JFrame frame = new MainFrame();
        while(!isExit){
        }
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }
}
