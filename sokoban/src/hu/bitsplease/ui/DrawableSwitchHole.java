package hu.bitsplease.ui;

import hu.bitsplease.service.SwitchHole;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * Kapcsoslyuk osztálya
 */
public class DrawableSwitchHole extends DrawableImmovableTileObject{

    /**
     * Zárt állapot képe
     */
    ImageIcon img_Closed;

    /**
     * hozzátartozó kapcsoslyuk
     */
    protected SwitchHole switchHole;

    DrawableSwitchHole(SwitchHole m_switchHole){
        img = new ImageIcon("img\\Hole_Closed.png");
        switchHole = m_switchHole;
    }

    DrawableSwitchHole() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    @Override
    public void draw() {
        if(switchHole.getS().IsSwitched()){
            setIcon(img);
        }
        else{
            setIcon(img_Closed);
        }
        this.setVisible(true);
    }

    @Override
    public int getDepth() {
        return 1;
    }
}
