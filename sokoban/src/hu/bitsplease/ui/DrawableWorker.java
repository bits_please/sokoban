package hu.bitsplease.ui;

import hu.bitsplease.service.Worker;

import javax.swing.*;
import java.io.InvalidClassException;

/**
 * A kirajzolandó mukások osztáya
 */
public class DrawableWorker extends DrawableMovableTileObject{

    /**
     * a megfelelő munkás
     */
    protected Worker worker;

    /**
     * konstruktor, ami kiválasztja a megfelelő képet
     * @param m_worker
     */
    DrawableWorker(Worker m_worker){
        img = new ImageIcon("img\\Worker.png");
        worker = m_worker;
    }

    DrawableWorker() throws InvalidClassException {
        throw new InvalidClassException("Not a valid Contructor");
    }

    /**
     * beállítja a kiválasztott képet
     */
    @Override
    public void draw() {
        this.setIcon(img);
        this.setVisible(true);
    }

    /**
     * visszatér a mélységgel
     * @return
     */
    @Override
    public int getDepth() {
        return 3;
    }
}

