package hu.bitsplease.ui;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A Tile osztály, ami JLayeredPane, így tudja kirajzolni a rajta tartózkodó objecteket
 */
public class DrawableTile extends JLayeredPane implements Drawable{

    /**
     * Tile képe
     */
    private static ImageIcon img;

    /**
     * A rajta lévő objectek listája
     */
    public List<DrawableTileObject> Drawables = new ArrayList<>();

    /**
     * konstruktor, kirajzolja magát
     */
    DrawableTile(){
         img = new ImageIcon("img\\Tile.png");
         JLabel bck = new JLabel();
         bck.setIcon(img);
         this.add(bck, new Integer(getDepth()));
    }

    /**
     * kirajzolja a rajta lévőket
     */
    @Override
    public void draw() {

        for(DrawableTileObject iterator : Drawables){
            this.add(iterator, new Integer(iterator.getDepth()));
            iterator.draw();
        }
    }

    @Override
    public int getDepth() {
        return 0;
    }
}
